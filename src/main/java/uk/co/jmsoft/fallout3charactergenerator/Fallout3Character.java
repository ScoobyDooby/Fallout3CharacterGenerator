/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.co.jmsoft.fallout3charactergenerator;

import java.util.Map;
import java.util.Set;

/**
 *
 * @author Scooby
 */
public class Fallout3Character
{

    Map<Fallout3Attribute, Integer> attributes;

    Set<Fallout3Skill> skills;

    protected void setAttribs(Map<Fallout3Attribute, Integer> attribs)
    {
        this.attributes = attribs;
    }

    public void setSkills(Set<Fallout3Skill> skills)
    {
        this.skills = skills;
    }

    @Override
    public String toString()
    {
        StringBuilder result = new StringBuilder();
        result.append("[Attributes]").append("\n");
        for (Fallout3Attribute eachAttrib : Fallout3Attribute.values())
        {
            result.append(eachAttrib).append("\t ");
            if (attributes.get(eachAttrib).toString().length() == 1)
            {
                result.append("  ");
            }
            result.append(attributes.get(eachAttrib)).append("\n");
        }
        result.append("[Skills]").append("\n");
        for (Fallout3Skill eachSkill : skills)
        {
            result.append(eachSkill.toString()).append("\n");
        }
        return result.toString();
    }

}
