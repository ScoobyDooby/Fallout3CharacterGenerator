/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.co.jmsoft.fallout3charactergenerator;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Scooby
 */
public class Main
{

    public static void main(String[] args)
    {
        Fallout3CharacterGenerator chargen = new Fallout3CharacterGenerator();
        for (int i = 0; i < 100; i++)
        {
            Fallout3Character character;
            try
            {
                character = chargen.generateCharacter();
            }
            catch (ImpossibleCharacterException ex)
            {
                throw new IllegalStateException(ex);
            }
            System.out.println(character);
        }
    }
}
