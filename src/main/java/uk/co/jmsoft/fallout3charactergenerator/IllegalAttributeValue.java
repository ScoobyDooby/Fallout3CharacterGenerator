/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.co.jmsoft.fallout3charactergenerator;

/**
 *
 * @author Scooby
 */
public class IllegalAttributeValue extends Exception
{

    public IllegalAttributeValue(String message)
    {
        super(message);
    }

    public IllegalAttributeValue(String message, Throwable cause)
    {
        super(message, cause);
    }

    public IllegalAttributeValue(Throwable cause)
    {
        super(cause);
    }
}
