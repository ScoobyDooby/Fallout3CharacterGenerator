/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.co.jmsoft.fallout3charactergenerator;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultListModel;
import javax.swing.JCheckBox;
import javax.swing.JOptionPane;
import javax.swing.JSpinner;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

/**
 *
 * @author Scooby
 */
public class GUI extends javax.swing.JFrame
{

    Fallout3CharacterGenerator generator;
    Map<Fallout3Attribute, AttributeMinMax> attributes = new HashMap<>();
    Map<Fallout3Skill, JCheckBox> skills = new HashMap<>();

    /**
     * Creates new form GUI
     */
    public GUI()
    {
        initComponents();
        this.setTitle("Fallout 3 Character Generator");
        ((GridLayout) attributePanel.getLayout()).setColumns(1);
        ((GridLayout) attributePanel.getLayout()).setRows(0);

        generator = new Fallout3CharacterGenerator();
        // Generate atribute controls
        for (Fallout3Attribute eachAttribute : Fallout3Attribute.values())
        {
            AttributeMinMax minmax = new AttributeMinMax();
            minmax.getLabel().setText(eachAttribute.toString());
            minmax.getMinSpinner().addChangeListener(new ChangeListener()
            {
                @Override
                public void stateChanged(ChangeEvent e)
                {
                    try
                    {
                        generator.setMin(eachAttribute, (Integer) ((JSpinner) e.getSource()).getValue());
                    }
                    catch (IllegalAttributeValue ex)
                    {
                        update();
                        JOptionPane.showMessageDialog(rootPane, ex.getMessage());
                    }
                }
            });
            minmax.getMaxSpinner().addChangeListener(new ChangeListener()
            {
                @Override
                public void stateChanged(ChangeEvent e)
                {
                    try
                    {
                        generator.setMax(eachAttribute, (Integer) ((JSpinner) e.getSource()).getValue());
                    }
                    catch (IllegalAttributeValue ex)
                    {
                        update();
                        JOptionPane.showMessageDialog(rootPane, ex.getMessage());
                    }
                }
            });
            attributes.put(eachAttribute, minmax);
            attributePanel.add(minmax);
        }

        //Generate skill checkboxes.
        ((GridLayout) skillPanel.getLayout()).setColumns(2);
        ((GridLayout) skillPanel.getLayout()).setRows(0);

        for (Fallout3Skill eachSkill : Fallout3Skill.values())
        {
            JCheckBox checkBox = new JCheckBox(eachSkill.toString());
            skills.put(eachSkill, checkBox);
            skillPanel.add(checkBox);
            checkBox.addActionListener(new ActionListener()
            {
                @Override
                public void actionPerformed(ActionEvent e)
                {
                    if (((JCheckBox) e.getSource()).isSelected())
                    {
                        try
                        {
                            generator.addSkill(eachSkill);
                        }
                        catch (CharacterException ex)
                        {
                            update();
                            JOptionPane.showMessageDialog(rootPane, ex.getMessage());
                        }
                    }
                    else
                    {
                        try
                        {
                            generator.removeSkill(eachSkill);
                        }
                        catch (CharacterException ex)
                        {
                            update();
                            JOptionPane.showMessageDialog(rootPane, ex.getMessage());
                        }
                    }
                }
            });
        }
        update();
    }

    public final void update()
    {
        for (Fallout3Attribute eachAttribute : Fallout3Attribute.values())
        {
            AttributeMinMax minmax = attributes.get(eachAttribute);
            minmax.getMinSpinner().getModel().setValue(generator.getMin(eachAttribute));
            minmax.getMaxSpinner().getModel().setValue(generator.getMax(eachAttribute));
        }
        for (Fallout3Skill eachSkill : skills.keySet())
        {
            skills.get(eachSkill).setSelected(generator.hasSkill(eachSkill));
        }

    }

    /**
     * This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {

        attributePanel = new javax.swing.JPanel();
        skillPanel = new javax.swing.JPanel();
        jPanel1 = new javax.swing.JPanel();
        generateButton = new javax.swing.JButton();
        filler1 = new javax.swing.Box.Filler(new java.awt.Dimension(0, 10), new java.awt.Dimension(0, 10), new java.awt.Dimension(32767, 10));
        filler2 = new javax.swing.Box.Filler(new java.awt.Dimension(0, 10), new java.awt.Dimension(0, 10), new java.awt.Dimension(32767, 10));
        jScrollPane1 = new javax.swing.JScrollPane();
        jEditorPane1 = new javax.swing.JEditorPane();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jMenuItem1 = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        attributePanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Attributes"));
        attributePanel.setMinimumSize(new java.awt.Dimension(205, 197));
        attributePanel.setLayout(new java.awt.GridLayout());

        skillPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Skills"));
        skillPanel.setMinimumSize(new java.awt.Dimension(205, 197));
        skillPanel.setLayout(new java.awt.GridLayout());

        generateButton.setText("Generate");
        generateButton.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                generateButtonActionPerformed(evt);
            }
        });
        jPanel1.add(generateButton);
        jPanel1.add(filler1);
        jPanel1.add(filler2);

        jScrollPane1.setViewportView(jEditorPane1);

        jMenu1.setText("File");

        jMenuItem1.setText("New");
        jMenuItem1.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jMenuItem1ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem1);

        jMenuBar1.add(jMenu1);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(attributePanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(18, 18, 18)
                        .addComponent(skillPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 586, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(attributePanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(skillPanel, javax.swing.GroupLayout.DEFAULT_SIZE, 197, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 238, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void generateButtonActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_generateButtonActionPerformed
    {//GEN-HEADEREND:event_generateButtonActionPerformed
        try
        {
            Fallout3Character character = generator.generateCharacter();
            jEditorPane1.setText(character.toString());
        }
        catch (ImpossibleCharacterException ex)
        {
            JOptionPane.showMessageDialog(rootPane, ex.getMessage());
        }
    }//GEN-LAST:event_generateButtonActionPerformed

    private void jMenuItem1ActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jMenuItem1ActionPerformed
    {//GEN-HEADEREND:event_jMenuItem1ActionPerformed
        generator = new Fallout3CharacterGenerator();
        update();
    }//GEN-LAST:event_jMenuItem1ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[])
    {
        /*
         * Set the Nimbus look and feel
         */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /*
         * If Nimbus (introduced in Java SE 6) is not available, stay with the
         * default look and feel.
         * For details see
         * http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html
         */
        try
        {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels())
            {
                if ("Nimbus".equals(info.getName()))
                {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        }
        catch (ClassNotFoundException ex)
        {
            java.util.logging.Logger.getLogger(GUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        catch (InstantiationException ex)
        {
            java.util.logging.Logger.getLogger(GUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        catch (IllegalAccessException ex)
        {
            java.util.logging.Logger.getLogger(GUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        catch (javax.swing.UnsupportedLookAndFeelException ex)
        {
            java.util.logging.Logger.getLogger(GUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /*
         * Create and display the form
         */
        java.awt.EventQueue.invokeLater(new Runnable()
        {
            public void run()
            {
                new GUI().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel attributePanel;
    private javax.swing.Box.Filler filler1;
    private javax.swing.Box.Filler filler2;
    private javax.swing.JButton generateButton;
    private javax.swing.JEditorPane jEditorPane1;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JPanel skillPanel;
    // End of variables declaration//GEN-END:variables
}
