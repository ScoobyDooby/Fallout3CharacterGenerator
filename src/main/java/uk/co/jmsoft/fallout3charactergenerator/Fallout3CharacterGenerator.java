/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.co.jmsoft.fallout3charactergenerator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Generates randomised starting character stats for Fallout 3.
 * Limits may be placed on the ranges of each primary attribute. Up to three
 * skills may also be fixed.
 * <p>
 * Instances of this class effectively form templates for generating
 * characters. The generateCharacter() method may be called repeatedly to
 * generate any number of random characters.</p>
 *
 * @author Scooby
 */
public class Fallout3CharacterGenerator
{

    // The absolute minimum attribute value.
    private int minAttribValue = 1;
    // The absolute maximum attribute value.
    private int maxAttribValue = 10;
    private int maxSkills = 3;
    private int minSkills = 3;
    private int totalPoints = 40;

    private final Map<Fallout3Attribute, Integer> maxValues = new HashMap<>();
    private final Map<Fallout3Attribute, Integer> minValues = new HashMap<>();
    private final Set<Fallout3Skill> skills = new HashSet<>();

    
    {
        for (Fallout3Attribute eachAttrib : Fallout3Attribute.values())
        {
            maxValues.put(eachAttrib, maxAttribValue);
            minValues.put(eachAttrib, minAttribValue);
        }
    }

    public void setMin(Fallout3Attribute attribute, int value) throws IllegalAttributeValue
    {
        if (value < minAttribValue)
        {
            throw new IllegalAttributeValue("Cannot set " + attribute + " below " + minAttribValue + ".");
        }
        if (value > maxAttribValue)
        {
            throw new IllegalAttributeValue("Cannot set " + attribute + " above " + maxAttribValue + ".");
        }
        if (value > maxValues.get(attribute))
        {
            throw new IllegalAttributeValue("Cannot set " + attribute + " higher than max value.");
        }
        int oldValue = minValues.get(attribute);
        minValues.put(attribute, value);
        try
        {
            checkTotalMins();
        }
        catch (ImpossibleCharacterException ex)
        {
            minValues.put(attribute, oldValue);
            throw new IllegalAttributeValue(ex.getMessage());
        }

    }

    public void setMax(Fallout3Attribute attribute, int value) throws IllegalAttributeValue
    {

        if (!(value >= minAttribValue))
        {
            throw new IllegalAttributeValue("Cannot set " + attribute + " below " + minAttribValue + ".");
        }
        if (!(value <= maxAttribValue))
        {
            throw new IllegalAttributeValue("Cannot set " + attribute + " above " + maxAttribValue + ".");
        }
        if (value < minValues.get(attribute))
        {
            throw new IllegalAttributeValue("Cannot set " + attribute + " lower than min value.");
        }
        int oldValue = maxValues.get(attribute);
        try
        {
            maxValues.put(attribute, value);
            checkIfPossible();
        }
        catch (ImpossibleCharacterException ex)
        {
            maxValues.put(attribute, oldValue);
            throw new IllegalAttributeValue(ex.getMessage());
        }
    }

    public int getMax(Fallout3Attribute attribute)
    {
        return maxValues.get(attribute);
    }

    public int getMin(Fallout3Attribute attribute)
    {
        return minValues.get(attribute);
    }

    /**
     * Check if the set min and max values and skills can create a legal
     * character.
     *
     * @throws ImpossibleCharacterException - if the character is not possible.
     */
    public void checkIfPossible() throws ImpossibleCharacterException
    {
        checkTotalMins();
        checkTotalMaxs();

        for (Fallout3Attribute eachAttrib : Fallout3Attribute.values())
        {
            if (minValues.get(eachAttrib) > maxValues.get(eachAttrib))
            {
                throw new ImpossibleCharacterException("Min value cannot exceed max value.");
            }
        }

    }

    private void checkTotalMins() throws ImpossibleCharacterException
    {
        int totalMins = 0;
        for (Integer eachMin : minValues.values())
        {
            totalMins += eachMin;
        }

        if (totalMins > totalPoints)
        {
            throw new ImpossibleCharacterException("Total of minimum attribute values must be greater than " + totalPoints + " points.");
        }
    }

    private void checkTotalMaxs() throws ImpossibleCharacterException
    {
        int totalMaxs = 0;
        for (Integer eachMax : maxValues.values())
        {
            totalMaxs += eachMax;
        }

        if (totalMaxs < totalPoints)
        {
            throw new ImpossibleCharacterException("Total of maximum attribute values must be greater than " + totalPoints + " points.");
        }
    }

    public Fallout3Character generateCharacter() throws ImpossibleCharacterException
    {
        checkIfPossible();

        Fallout3Character result = new Fallout3Character();
        // Randomise the list of attributes
        List<Fallout3Attribute> rndattributes = new ArrayList<>();
        rndattributes.addAll(Arrays.asList(Fallout3Attribute.values()));
        Collections.shuffle(rndattributes);
        int minTotal = 0;
        for (Integer eachMin : minValues.values())
        {
            minTotal += eachMin;
        }
        int remainingPoints = totalPoints - minTotal;
        int currentAttributeIndex = 0;
        Random random = new Random(System.nanoTime());
        Map<Fallout3Attribute, Integer> attribValues = new HashMap<>();
        attribValues.putAll(minValues);
        while (remainingPoints > 0)
        {
            Fallout3Attribute currentAttribute = rndattributes.get(currentAttributeIndex);
            int current = attribValues.get(currentAttribute);
            int max = maxValues.get(currentAttribute);
            if (current < max)
            {
                int maxrem = Math.min(max, current + remainingPoints);
                int rndint = 1 + random.nextInt(maxrem - current);
                attribValues.put(currentAttribute, attribValues.get(currentAttribute) + rndint);
                remainingPoints -= rndint;
            }
            // update index counter.
            currentAttributeIndex++;
            if (currentAttributeIndex > rndattributes.size() - 1)
            {
                currentAttributeIndex = 0;
            }
        }

        // Check correct number of skill points have been allocated
        int totalAllocated = 0;
        for (Integer eachAttrib : attribValues.values())
        {
            totalAllocated += eachAttrib;
        }

        assert totalAllocated == totalPoints;

        // Choose skills.
        Set<Fallout3Skill> tempSkills = new HashSet<Fallout3Skill>(skills);
        if (tempSkills.size() < maxSkills)
        {

            List<Fallout3Skill> rndSkills = new ArrayList<>();
            rndSkills.addAll(Arrays.asList(Fallout3Skill.values()));
            Collections.shuffle(rndSkills);

            while (tempSkills.size() < maxSkills && !rndSkills.isEmpty())
            {
                if (tempSkills.contains(rndSkills.get(0)))
                {
                    rndSkills.remove(0);
                }
                else
                {
                    tempSkills.add(rndSkills.remove(0));
                }
            }
        }

        result.setAttribs(attribValues);
        result.setSkills(tempSkills);
        return result;
    }

    public Set<Fallout3Skill> getSkills()
    {
        return new HashSet<>(skills);
    }

    public void addSkill(Fallout3Skill skill) throws CharacterException
    {
        if (skills.size() > maxSkills - 1)
        {
            throw new CharacterException("Maximum of " + maxSkills + " allowed.");
        }
        if (skills.contains(skill))
        {
            throw new CharacterException("Trying to add duplicate skill " + skill + ".");
        }
        skills.add(skill);
    }

    public void removeSkill(Fallout3Skill skill) throws CharacterException
    {
        if (!skills.contains(skill))
        {
            throw new CharacterException("Skill " + skill + " not present in character.");
        }
        skills.remove(skill);
    }

    public boolean hasSkill(Fallout3Skill skill)
    {
        return skills.contains(skill);
    }

}
